# F1 Race
An app to show all the F1 Race information

# My views and strategies
## Frotend
Used ES6,Webpack,Reactjs and Reduxjs for this project. On Using Reduxjs, avoided mutable updates to the data. Also made a good minimalist UI design. Used CSS3 Flexbox module like sticky footer and for other usage.
Throughout the project, respected errors (i.e properly showing) and showing spinners wherever is necessary.

# Getting Started to run locally

Clone the repository. Install dependencies with:

``npm install``

alternatively for faster dependency downloads do

``yarn install`` if you have yarn installed

# Run Server
After completing all above steps run your node.js server
```
npm start
```
# Now open 
http://localhost:8080

# Getting Started by docker
Run following commands from the root of the directory to pick up Dockerfile

```
docker build -t <tagname> .
docker run -d -p 8080:8080 <tagname>
```
which runs a nodejs server on port on 8080. End point is http://<docker-machine-ip>:8080 

## Love :heart: to hear feedback from you
RT Bathula-weirdo,coffee lover
battu.network@gmail.com
